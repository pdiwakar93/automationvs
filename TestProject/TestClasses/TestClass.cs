﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using log4net;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;
using TestFramework.Collections;
using TestFramework.ExcelDriver;
using TestFramework.Logs;
using TestFramework.Screenshots;
using TestProject.PageObject;

namespace TestProject
{
    public class TestClass
    {
        string path;
        ExcelLibrary lib;
        string method;
        ILog Logger = Log.GetLogger(typeof(TestClass));
        public ExtentReports extent;
        public ExtentTest test;
        public ExtentHtmlReporter avent;

        [OneTimeSetUp]
        public void OneTimeData()
        {
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath;
            string reportPath = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName + "\\TestFramework\\Reports\\";
            extent = new ExtentReports();

            extent.AddSystemInfo("Host Name", "Kavita");
            extent.AddSystemInfo("Environment", "QA");
            extent.AddSystemInfo("User Name", "kagrahari");
            avent = new ExtentHtmlReporter(reportPath);
            extent.AttachReporter(avent);

        }

        [SetUp]
        public void SetUp()
        {
            Session.driver = new ChromeDriver();
            Logger.Info($"Crome Driver Path is" + @"'C:\Users\Diwakar Paharia\Documents\Visual Studio 2017'");
            Session.driver.Navigate().GoToUrl("http://demo.guru99.com/V4/index.php");
            Session.driver.Manage().Window.Maximize();
            Session.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            string snapshot = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;
            path = snapshot + "\\TestData\\TestDataExl.xlsx";
            lib = new ExcelLibrary();
        }

        [Test]
        public void LoginGuru99Failed()
        {
            method = string.Format(MethodBase.GetCurrentMethod().Name);
            test = extent.CreateTest(method);
            var username = lib.GetExcelData(path, 2, 2);
            test.Info($"Username to be {username}");
            var password = lib.GetExcelData(path, 2, 3);
            test.Info($"Password to be {password}");
            var loginPage = new LoginPage();
            test.Info($"Login in with the username and password");
            var title = loginPage.Login(username, password);
            test.Info($"Title of Page is {title}");
            Assert.AreEqual("ABC", title, $"Title of web page is {title}");
        }


        [Test]
        public void LoginGuru99Pass()
        {
            method = string.Format(MethodBase.GetCurrentMethod().Name);
            test = extent.CreateTest(method);
            var username = lib.GetExcelData(path, 2, 2);
            test.Info($"Username to be {username}");
            var password = lib.GetExcelData(path, 2, 3);
            test.Info($"Password to be {password}");
            var loginPage = new LoginPage();
            test.Info($"Login in with the username and password");
            var title = loginPage.Login(username, password);
            test.Info($"Title of Page is {title}");
            Assert.AreEqual("Guru99 Bank Manager HomePage", title, $"Title of web page is {title}");
        }

        [TearDown]
        public void TearDown()
        {
            var errorMessage = TestContext.CurrentContext.Result.Message;
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Failure)
            {
                var ssPath = TakeScreenshot.TakeSnapshot(method);
                test.AddScreenCaptureFromPath(ssPath);
                test.Log(Status.Fail, "Snapshot below: " + ssPath);
                test.Log(Status.Fail, errorMessage);
            }
            else
            {
                test.Log(Status.Pass);
            }
            Session.driver.Quit();
        }
        [OneTimeTearDown]
        public void EndReport()
        {
            extent.Flush();
        }
    }
}
