﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using TestFramework.Collections;

namespace TestProject.PageObject
{
    class HomePage
    {
        [FindsBy(How = How.Name, Using = "uid")]
        public IWebElement UserName { get; set; }

        public HomePage()
        {
            PageFactory.InitElements(Session.driver, this);
        }

    }
}
