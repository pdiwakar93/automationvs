﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestFramework.Collections;
using TestFramework.Warppers;

namespace TestProject.PageObject
{
    class LoginPage
    {
        [FindsBy(How = How.Name, Using = "uid")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.Name, Using = "password")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Name, Using = "btnLogin")]
        public IWebElement LoginBtn { get; set; }

        public LoginPage()
        {
            PageFactory.InitElements(Session.driver, this);
        }
        public string Login(string username, string password)
        {
            SetClass.EnterText(UserName, username);
            SetClass.EnterText(Password, password);
            SetClass.Click(LoginBtn);
            return GetClass.GetTitle(Session.driver);
        }

    }
}
