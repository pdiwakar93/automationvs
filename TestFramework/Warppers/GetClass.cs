﻿using log4net;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework.Logs;

namespace TestFramework.Warppers
{
    public class GetClass
    {
        static ILog Logger = Log.GetLogger(typeof(GetClass));
        public static string GetText(IWebDriver driver, By by)
        {
            Logger.Info(driver.FindElement(by).Text);
            return driver.FindElement(by).Text;
            
        }
        public static string GetText(IWebElement element)
        {
            Logger.Info(element.Text);
            return element.Text;
        }
        public static bool VerifyText(string expected, string actual)
        {
            return expected.Equals(actual) ? true : false;
        }
        public static string GetTitle(IWebDriver driver)
        {
            return driver.Title;
        }
    }
}
