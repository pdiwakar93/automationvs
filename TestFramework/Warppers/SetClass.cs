﻿using log4net;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestFramework.Collections;
using TestFramework.Logs;

namespace TestFramework.Warppers
{
    public class SetClass
    {
        static ILog Logger = Log.GetLogger(typeof(GetClass));
        public static void Click(By by)
        {
            Session.driver.FindElement(by).Click();
            Logger.Info(Session.driver.FindElement(by) + ".Click()");
        }
        public static void Click(IWebElement element)
        {
            element.Click();
        }
        public static void Click(Locator bytype, string locator)
        {
            if (bytype.Equals(Locator.Id))
            {
                Session.driver.FindElement(By.Id(locator)).Click();
                Logger.Info(Session.driver.FindElement(By.Id(locator)) + ".Click()");
            }
            if (bytype.Equals(Locator.Name))
            {
                Session.driver.FindElement(By.Name(locator)).Click();
                Logger.Info(Session.driver.FindElement(By.Name(locator)) + ".Click()");
            }
            if (bytype.Equals(Locator.Xpath))
            {
                Session.driver.FindElement(By.XPath(locator)).Click();
                Logger.Info(Session.driver.FindElement(By.XPath(locator)) + ".Click()");
            }
        }
        public static void EnterText(By by, string text)
        {
            Session.driver.FindElement(by).SendKeys(text);
            Logger.Info(Session.driver.FindElement(by) + $".Sendkeys({text})");
        }
        public static void EnterText(IWebElement element, string text)
        {
            element.SendKeys(text);
            Logger.Info(element+$".Sendkeys({text})");
        }
        public static void EnterText(Locator bytype, string locator, string text)
        {
            if (bytype.Equals(Locator.Id))
                Session.driver.FindElement(By.Id(locator)).SendKeys(text);
            if (bytype.Equals(Locator.Name))
                Session.driver.FindElement(By.Name(locator)).SendKeys(text);
            if (bytype.Equals(Locator.Xpath))
                Session.driver.FindElement(By.XPath(locator)).SendKeys(text);
        }
    }
}
