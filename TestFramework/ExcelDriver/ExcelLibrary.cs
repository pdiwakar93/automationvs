﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using System.Linq;
using System.Data;

namespace TestFramework.ExcelDriver
{
    public class ExcelLibrary
    {
        public string GetExcelData(string DataFileName, int row, int column)
        {
            
            Application appClass = new Application();
            IntPtr hwnd = new IntPtr(appClass.Hwnd);
            Workbook workBook = appClass.Workbooks.Open(DataFileName, Type.Missing, Type.Missing, Type.Missing,
                                                      Type.Missing, Type.Missing, Type.Missing,
                                                      Type.Missing, Type.Missing, Type.Missing,
                                                      Type.Missing, Type.Missing, Type.Missing,
                                                      Type.Missing, Type.Missing);

            Sheets sheets = workBook.Worksheets;
            Worksheet worksheet = (Worksheet)sheets.get_Item(1);

            //Range firstColumn = worksheet.UsedRange.Columns[1];
            //System.Array myvalues = (System.Array)firstColumn.Cells.Value;

            //string[] strArray = myvalues.OfType<object>().Select(o => o.ToString()).ToArray();

            Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)worksheet.UsedRange;
            //int rw = range.Rows.Count;
            //int cl = range.Columns.Count;
            var v = Convert.ToString((range.Cells[row, column] as Microsoft.Office.Interop.Excel.Range).Value2);
            workBook.Close();
            return v.Trim();
        }
    }
}
