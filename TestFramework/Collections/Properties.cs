﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework.Collections
{
    public enum Locator
    {
        Id,
        Xpath,
        Name,
        ClassName,
        LinkText,
        TagName,
        PartialLinkText,
        CSSSelector
    }
    class Properties
    {
    }
}
