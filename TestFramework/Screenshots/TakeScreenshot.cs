﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework.Collections;

namespace TestFramework.Screenshots
{
    public static class TakeScreenshot
    {
        public static string TakeSnapshot(string testcaseName)
        {
            var screenshotDriver = Session.driver as ITakesScreenshot;
            var screenshot = screenshotDriver.GetScreenshot();
            string snapshot = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName + "\\screenshot";
            if (!System.IO.Directory.Exists(snapshot))
            {
                System.IO.Directory.CreateDirectory(snapshot);
            }
            snapshot = snapshot + "\\" + testcaseName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".png";
            screenshot.SaveAsFile(snapshot, ScreenshotImageFormat.Png);
            return snapshot;
        }
    }
}
